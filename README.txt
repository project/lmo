
Description
-----------
This module integrates the famous 'Liga Manager Online' into your Drupal installation.

Warnings
--------
The module is in rare alpha state. Only for testing purposes! Please give feedback at the Drupal project page at http://drupal.org/project/lmo!
I only really tested it with clean_urls enabled, so send issues if you don't use clean_urls an find any bug!

Installation
------------
1) Go to http://www.liga-manager-online.de, download LMO4, install it as described and change admin password to your needs. You also can donwload LMO from http://drupaldev.planet-berlin-it.de !
2) Upload the whole folder to your drupal installation under '/modules'.
3) enable the 'lmo module' in drupal by visiting '/admin/modules'
4) visit '/admin/settings/lmo' to set the path to your local LMO installation (not the url!) and the admin account information

Usage
-----
1) go to '/lmo' to watch if the preinstalled leagues will show up
2) if no leagues are shown please break here an submit a report to the project page!
2) got to 'admin/content/lmo' to create and administer your own leagues

Plans
-----
1) Filter to insert leagues, standings and match results into your nodes
2) Connection to 'og_teampage' module to show up standings and match results into your teampages
3) Blocks with standings, leaques etc